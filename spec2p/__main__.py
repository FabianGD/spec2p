from pathlib import Path
import argparse
import numpy as np


def argparsing():
    parser = argparse.ArgumentParser(description="Cuts spectra in pieces.")
    parser.add_argument(
        "inputfile",
        help=(
            "Input file, either a serial spectrum file (with 4 columns)"
            "or a single spectrum file (with 3 columns). "
        ),
        nargs="+",
        type=str,
    )

    parser.add_argument(
        "-o",
        "--outputfolder",
        help=(
            "Where to put the resulting files. "
            "May be an absolute or relative path"
        ),
        default="out/",
    )
    parser.add_argument(
        "-p",
        "--pattern",
        help=(
            "Filename base for the output filename. "
            "Will be suffixed with '_$NR$_1.txt'"
        ),
        default="same",
    )
    parser.add_argument("-w", "--wavenumber", help="Wavenumber cutoff (highpass)")

    return parser


def main():

    args = argparsing().parse_args()

    arg_pattern = args.pattern

    if args.wavenumber is not None:
        cutoff = float(args.wavenumber)
    else:
        cutoff = None

    files = sorted([Path(ii).resolve() for ii in args.inputfile])
    ofolder = Path(args.outputfolder).resolve()
    ofolder.mkdir(parents=True, exist_ok=True)

    for file in files:
        file = file.resolve()
        print(file.name)
        data = np.loadtxt(file, delimiter="\t", dtype=float)

        if arg_pattern == "same":
            pattern = file.stem

        if data.shape[-1] == 4:
            # ! What does it do?
            proc = np.split(
                data[:, [0, 2, 3]],
                np.cumsum(np.unique(data[:, 2], return_counts=True)[1])
            )
            fname_pattern = "{patt}_{idx:03d}_1.txt"

        elif data.shape[-1] == 3:
            # nr = int(file.stem.split("_")[-1])
            proc = [data]
            fname_pattern = "{patt}_proc_1.txt"

        for ii, dataset in enumerate(proc):
            if cutoff is not None:
                mask = dataset[:, 0] > cutoff
                dataset = dataset[mask]

            if data.shape[0] != 0:
                fname = ofolder / fname_pattern.format(
                    patt=pattern, idx=(ii + 1)
                )
                np.savetxt(fname, dataset, fmt="%.4f\t%.0f\t%.0f")


if __name__ == "__main__":
    main()
