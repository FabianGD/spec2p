# clean_spec
from pathlib import Path
import argparse

parser = argparse.ArgumentParser(description="Cleans a serial spectrum for processing.")
parser.add_argument("inputfile", help="Input file")
args = parser.parse_args()

file = Path(args.inputfile)
newfile = file.parent / file.stem + "_proc.txt"

with open(file, "r") as f:
    data = f.read().replace(",", ".")

with open(newfile, "w") as f:
    f.write(data)
