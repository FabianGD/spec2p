from setuptools import setup

setup(
    name="spec2p",
    version="0.0.2",
    packages=['spec2p'],
    entry_points={
        "console_scripts": [
            "spec2p = spec2p.__main__:main",
        ]
    }
)
